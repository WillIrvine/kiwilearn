#!/usr/bin/python
from wtforms import Form, StringField, validators, TextAreaField, IntegerField

class SubmitForm(Form):
    name = StringField("Tool Name", [validators.Length(min=2, max=25), validators.DataRequired()])
    website = StringField("Website URL (no 'http://')", [validators.Length(min=5, max=60), validators.DataRequired()])
    min_age = IntegerField("Minimum Age", [validators.DataRequired()])
    max_age = IntegerField("Maximum Age", [validators.DataRequired()])
    description = TextAreaField("Description", [validators.DataRequired()])
    img_url = StringField("URL for Image", [validators.Length(min=5, max=200)])

class Search(Form):
    query = StringField("Your Search")
