#!/usr/bin/python
import sqlite3
import sys, os
from ..config import Config

config = Config()


class Tool:
    def __init__(self):
        self.dbPath = "/root/kiwilearn/kiwilearn/database/database.db"
    def addTool(self, name, website, min_age, max_age, description, img_url):
        with sqlite3.connect(self.dbPath) as con:
            cur = con.cursor() 
            cur.execute("SELECT * FROM Tools WHERE name = ? OR website = ?", [name, website])
            result = cur.fetchone()
            if result:
                return False
            else:
                cur = con.cursor()
                cur.execute("INSERT INTO Tools (name, website, min_age, max_age, description, img_url) VALUES (?, ?, ?, ?, ?, ?)", (name, website, min_age, max_age, description, img_url))
                con.commit()
                return True
    def getTool(self, id):
        with sqlite3.connect(self.dbPath) as con:
            cur = con.cursor()
            cur.execute("SELECT * FROM Tools WHERE id = ?", [id])
            result = cur.fetchone()
            return result
    def getToolsFromName(self, name):
        with sqlite3.connect(self.dbPath) as con:
            cur = con.cursor() 
            funkyname = "%" + name + "%"
            cur.execute("SELECT * FROM Tools WHERE name LIKE ?", [funkyname])
            result = cur.fetchall()
            return result
    def getAllTools(self):
        with sqlite3.connect(self.dbPath) as con:
            cur = con.cursor()
            cur.execute("SELECT * FROM Tools")
            result = cur.fetchall()
            return result



    