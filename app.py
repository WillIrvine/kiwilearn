#!/usr/bin/python
from flask import Flask, render_template, request, redirect, flash
from .classes.tool import Tool
from .classes.forms import SubmitForm, Search
from .config import Config

config = Config()
def create_app():
  app = Flask(__name__)
  return app
app = create_app()
app.secret_key = config.secret_key
app.config['TEMPLATES_AUTO_RELOAD'] = True

@app.route('/')
def home():
    return render_template("home.html")

@app.route('/tutorial')
def tutorial():
    return render_template("tutorial.html")

@app.route('/tools')
def tools():
    f = Tool()
    result = f.getAllTools()
    return render_template("tools.html", tools=result)

@app.route('/submit', methods=["GET", "POST"])
def submit():
    form = SubmitForm(request.form)
    if request.method == "POST" and form.validate():
        name = form.name.data
        website = form.website.data
        min_age = form.min_age.data
        max_age = form.max_age.data
        description = form.description.data
        img_url = form.img_url.data
        f = Tool()
        success = f.addTool(name, website, min_age, max_age, description, img_url)
        if success:
            flash("Tool added")
            return render_template("home.html")
        else:
            flash("There was a problem with the system")
            return render_template("submit.html", form=form)
    return render_template("submit.html", form=form)

@app.route('/about')
def about():
    return render_template("about.html")


@app.route("/tool/<int:id>")
def tool(id):
    f = Tool()
    result = f.getTool(id)
    if result == None:
        abort(404)
    else:
        name = result[1]
        website = result[2]
        description = result[6]
        min_age = result[4]
        max_age = result[5]
        img_url = result[7]
        return render_template("tool.html", name=name, website=website, description=description, min_age=min_age, max_age=max_age, img_url=img_url)

@app.route("/search", methods=["GET", "POST"])
def search():
    form = Search(request.form)
    if request.method == "POST" and form.validate():
        query = form.query.data
        f = Tool()
        success = f.getToolsFromName(query)
        if success:
            return render_template("search_results.html", tools=success)
        else:
            return render_template("search.html", form=form, results=False)
    return render_template("search.html", form=form, results=True)


    return render_template("search.html")


@app.errorhandler(404)
def page_not_found(error):
   return render_template('404.html', title = '404'), 404

if __name__ == "__main__":
    app.run(host='0.0.0.0')