CREATE TABLE Tools (
	id integer PRIMARY KEY AUTOINCREMENT,
	name text,
	website text,
	verified boolean,
	min_age integer,
	max_age integer,
	description text,
    img_url text
);
